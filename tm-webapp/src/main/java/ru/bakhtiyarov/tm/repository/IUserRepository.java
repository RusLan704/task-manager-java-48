package ru.bakhtiyarov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.entity.User;

public interface  IUserRepository extends IRepository<User> {

    void deleteByLogin(@NotNull final String login);

    @Nullable
    User findByLogin(@NotNull final String login);

}
