package ru.bakhtiyarov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.bakhtiyarov.tm.api.service.IProjectService;
import ru.bakhtiyarov.tm.api.service.ITaskService;
import ru.bakhtiyarov.tm.api.service.IUserService;
import ru.bakhtiyarov.tm.entity.Project;
import ru.bakhtiyarov.tm.entity.Task;
import ru.bakhtiyarov.tm.exception.empty.EmptyDescriptionException;
import ru.bakhtiyarov.tm.exception.empty.EmptyUserIdException;
import ru.bakhtiyarov.tm.exception.empty.EmptyNameException;
import ru.bakhtiyarov.tm.exception.invalid.InvalidProjectException;
import ru.bakhtiyarov.tm.exception.invalid.InvalidTaskException;
import ru.bakhtiyarov.tm.exception.system.IncorrectIndexException;
import ru.bakhtiyarov.tm.repository.ITaskRepository;

import java.util.List;

@Service
public class TaskService extends AbstractService<Task, ITaskRepository> implements ITaskService {

    @NotNull
    private final IProjectService projectService;

    @NotNull
    private final IUserService userService;

    @NotNull
    private final ITaskRepository taskRepository;

    @NotNull
    @Autowired
    public TaskService(
           @NotNull final IProjectService projectService,
           @NotNull final IUserService userService, 
           @NotNull final ITaskRepository taskRepository
    ) {
        this.projectService = projectService;
        this.userService = userService;
        this.taskRepository = taskRepository;
    }

    @SneakyThrows
    @Transactional
    public Task create(@Nullable final String userId, @Nullable final Task task, @Nullable final String projectId) {
        if (userId == null) throw new InvalidTaskException();
        if (task == null) throw new InvalidTaskException();
        if (task.getName() == null || task.getName().isEmpty()) throw new EmptyNameException();
        if (task.getDescription() == null || task.getDescription().isEmpty()) throw new EmptyNameException();
        @Nullable final Project project = projectService.findById(projectId);
        if (project == null) throw new InvalidProjectException();
        task.setUser(userService.findById(userId));
        task.setProject(project);
        return taskRepository.save(task);
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Task updateTaskById(
            @Nullable final String userId,
            @Nullable final Task task
    ) {
        if (userId == null) throw new EmptyUserIdException();
        if (task == null) throw new InvalidTaskException();
        @Nullable final String id = task.getId();
        @Nullable final String name = task.getName();
        @Nullable final String description = task.getDescription();
        if (id == null || id.isEmpty()) throw new EmptyUserIdException();
        if (name == null) throw new EmptyNameException();
        if (description == null) throw new EmptyDescriptionException();
        @Nullable final Task taskUpdated = findOneById(userId, id);
        if (taskUpdated == null) throw new InvalidTaskException();
        taskUpdated.setId(id);
        taskUpdated.setName(name);
        taskUpdated.setDescription(description);
        taskUpdated.setStatus(task.getStatus());
        taskUpdated.setStartDate(task.getStartDate());
        taskUpdated.setFinishDate(task.getFinishDate());
        return save(taskUpdated);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAll(@Nullable final String userId) {
        if (userId == null) throw new EmptyUserIdException();
        return taskRepository.findAllByUserId((userId));
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task findOneById(@Nullable final String userId, @Nullable String id) {
        if (userId == null) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyUserIdException();
        return taskRepository.findByUserIdAndId(userId, id);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task findOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        return taskRepository.findAllByUserId(userId).get(index);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task findOneByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.findByUserIdAndName(userId, name);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAllByUserIdAndProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyUserIdException();
        @Nullable final Project project = projectService.findOneById(userId, projectId);
        if (project == null) throw new InvalidProjectException();
        return taskRepository.findAllByUserIdAndProjectId(userId, projectId);
    }

    @Override
    public Task findById(@Nullable final String id) {
        if(id == null || id.isEmpty()) throw new EmptyUserIdException();
        return taskRepository.findById(id).get();
    }

    @NotNull
    @Override
    @PreAuthorize("hasRole('ADMIN')")
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @SneakyThrows
    @Transactional
    public void removeAll(@Nullable final String userId) {
        if (userId == null) throw new EmptyUserIdException();
        taskRepository.deleteAllByUserId(userId);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        @Nullable Task task = taskRepository.findAllByUserId((userId)).get(index);
        if(task == null) throw new InvalidTaskException();
        taskRepository.deleteById(task.getId());
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeOneById(@Nullable final String userId,  @Nullable final String id) {
        if (userId == null) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyUserIdException();
        taskRepository.deleteByUserIdAndId(userId, id);
    }

    @Override
    @PreAuthorize("hasRole('ADMIN')")
    public void removeOneById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new EmptyUserIdException();
        taskRepository.deleteById(id);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeOneByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        taskRepository.deleteByUserIdAndName(userId, name);
    }

    @Override
    @Transactional
    public void removeAll() {
        taskRepository.deleteAll();
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeAllByUserIdAndProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyUserIdException();
        @Nullable final Project project = projectService.findOneById(userId, projectId);
        if (project == null) throw new InvalidProjectException();
        taskRepository.deleteAllByUserIdAndProjectId(((userId)), projectId);
    }

    @NotNull
    @Override
    protected ITaskRepository getRepository() {
        return taskRepository;
    }

}