package ru.bakhtiyarov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.bakhtiyarov.tm.endpoint.soap.ProjectDTO;
import ru.bakhtiyarov.tm.endpoint.soap.ProjectSoapEndpoint;
import ru.bakhtiyarov.tm.event.ConsoleEvent;
import ru.bakhtiyarov.tm.listener.AbstractListener;
import ru.bakhtiyarov.tm.util.TerminalUtil;

@Component
public final class ProjectUpdateByIdListener extends AbstractListener {

    @NotNull
    @Autowired
    private ProjectSoapEndpoint projectEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "project-update-by-id";
    }

    @NotNull
    @Override
    public String description() {
        return "Update project by id.";
    }

    @EventListener(condition = "@projectUpdateByIdListener.name() == #event.name")
    public void handler(final ConsoleEvent event) {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @Nullable final ProjectDTO project = projectEndpoint.findProjectById(id);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final ProjectDTO newProject = new ProjectDTO();
        newProject.setId(id);
        newProject.setName(name);
        newProject.setDescription(description);
        @NotNull final ProjectDTO projectUpdated = projectEndpoint.updateProjectById(newProject);
        if (projectUpdated == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

}