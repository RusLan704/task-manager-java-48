package ru.bakhtiyarov.tm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@EnableZuulProxy
@SpringBootApplication
public class ProxyServer {

    public static void main(String[] args) {
        SpringApplication.run(ProxyServer.class, args);
    }

}
