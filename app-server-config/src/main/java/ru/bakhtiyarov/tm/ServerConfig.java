package ru.bakhtiyarov.tm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

/**
 * http://localhost:8888/app-server-tm.properties
 *
 */
@EnableConfigServer
@SpringBootApplication
public class ServerConfig {

    public static void main(String[] args) {
        SpringApplication.run(ServerConfig.class, args);
    }

}
